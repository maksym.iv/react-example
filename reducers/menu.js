import {
  MENU__SET_LOADING,
  MENU__ADD_BANNER
} from 'actionTypes/menu';

const initialState = {
  menuBanner: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case MENU__SET_LOADING: {
      return {
        ...state,
        loading: action.payload
      };
    }
    case MENU__ADD_BANNER: {
      return {
        ...state,
        menuBanner: {
          ...state.menuBanner,
          ...action.payload
        }
      };
    }
    default:
      return state;
  }
};
