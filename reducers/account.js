import { ACCOUNT__SET_ACCOUNT_INFO, ACCOUNT__SET_LOADING } from 'actionTypes/account';

const initialState = {
  accountInfo: {
    accountId: '',
    email: ''
  },
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ACCOUNT__SET_ACCOUNT_INFO: {
      return {
        ...state,
        accountInfo: {
          ...state.accountInfo,
          ...action.payload,
        }
      };
    }
    case ACCOUNT__SET_LOADING: {
      return {
        ...state,
        loading: action.payload
      };
    }
    default:
      return state;
  }
};
