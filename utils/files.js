const showImageUrl = (value) => {
    if (typeof value === 'string' && !!value.length) return value
    return convertToBlobUrl(value);
}

const getImgName = (img) => {
    const isString = typeof img === 'string';
    const imageArray = isString ? img.split('/') : img.name.split('/');

    return imageArray[imageArray.length - 1].replace(/\.[^/.]+$/, '');
}

const firstEntry = (obj) => Object.keys(obj)[0];


export { showImageUrl, getImgName, firstEntry };
