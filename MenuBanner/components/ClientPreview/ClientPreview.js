import React from 'react';
import PropTypes from 'prop-types';
import useStyles from './styles';

const ClientPreview = ({
  bannerImage,
  categories
}) => {
  const classes = useStyles();

  return (
    <div
      className={classes.clientPreview}>
      <div className={classes.homepage}>
        <header>
          <div className='icon backBtn'>
            <svg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 24 24'>
              <path d='M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z' />
            </svg>
          </div>
          <div className='icon search'>
            <svg viewBox='0 0 20 20' fill='none' width='1em' height='1em' xmlns='http://www.w3.org/2000/svg'>
              <g>
                <path
                  fillRule='evenodd'
                  clipRule='evenodd'
                  d='M12.9167 11.6667H12.2583L12.025 11.4417C12.8417 10.4917 13.3333 9.25833 13.3333 7.91667C13.3333 4.925 10.9083 2.5 7.91667 2.5C4.925 2.5 2.5 4.925 2.5 7.91667C2.5 10.9083 4.925 13.3333 7.91667 13.3333C9.25833 13.3333 10.4917 12.8417 11.4417 12.025L11.6667 12.2583V12.9167L15.8333 17.075L17.075 15.8333L12.9167 11.6667ZM7.91667 11.6667C5.84167 11.6667 4.16667 9.99167 4.16667 7.91667C4.16667 5.84167 5.84167 4.16667 7.91667 4.16667C9.99167 4.16667 11.6667 5.84167 11.6667 7.91667C11.6667 9.99167 9.99167 11.6667 7.91667 11.6667Z'
                  fill='currentColor' />
              </g>
            </svg>
          </div>
          <div className='icon'>
            <svg viewBox='0 0 24 24' fill='none' width='1em' height='1em' xmlns='http://www.w3.org/2000/svg'>
              <g>
                <path
                  fillRule='evenodd'
                  clipRule='evenodd'
                  d='M3 18H21V16H3V18ZM7 13H21V11H7V13ZM3 6V8H21V6H3Z'
                  fill='currentColor'
                />
              </g>
            </svg>
          </div>
        </header>

        {!!bannerImage &&
          <div className='banner'>
            <img src={encodeURI(bannerImage)} alt='previewBanner' />
          </div>
        }

        <div className='title'>
          <div className='textBox'>
            <p>Основное меню</p>
          </div>

          <div className='menuBtn'>
            <svg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 24 26'>
              <path d='M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z' />
            </svg>
          </div>
        </div>

        <div className='categories'>
          {categories.map(category => (
            <div key={category._id} className='category'>
              <div className='img' key={category._id}>
                <img src={category.image} alt='categoryImg' />
              </div>
              <p>{category.name.uk}</p>
            </div>
          ))}
        </div>

      </div>
    </div>
  );
};

ClientPreview.defaultProps = {
  bannerImage: '',
  categories: [],
};

ClientPreview.propTypes = {
  bannerImage: PropTypes.string,
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      typeId: PropTypes.string,
      name: PropTypes.instanceOf(Object),
    }),
  ),
};

export default ClientPreview;