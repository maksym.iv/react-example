import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  clientPreview: {
    width: 200,
    height: 375,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 24,
    borderRadius: 12,
    backgroundColor: '#fff',
    backgroundPosition: 'top',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  homepage: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: 8,
    overflow: 'hidden',
    '& header': {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      '& .icon': {
        padding: '0 6px',
      },
      '& .search': {
        marginLeft: 'auto',
      },
    },
    '& .backBtn': {
      color: '#1d1d1d',
    },
    '& .banner': {
      flexShrink: 0,
      height: 100,
      marginTop: 8,
      marginBottom: 8,
      border: '1px solid #ededed',
      borderRadius: 8,
      overflow: 'hidden',
      '& img': {
        width: '100%',
        height: '100%',
        objectFit: 'cover',
      },
    },
    '& .title': {
      display: 'flex',
      '& .textBox': {
        textAlign: 'center',
        width: '100%',
        alignSelf: 'center',
        '& p': {
          textAlign: 'center',
          fontFamily: 'Roboto',
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: 10,
          color: '#1d1d1d',
          margin: 0,
          paddingLeft: '22x',
        },
      },
      '& .menuBtn': {
        borderRadius: 7,
        border: '1px solid #ededed',
        overflow: 'hidden',
        padding: '0 5px',
      },
    },
    '& .menuIcon': {
      position: 'absolute',
      top: 194,
      right: 10,
      borderRadius: 14,
      border: '2px solid #ededed',
      overflow: 'hidden',
    },
    '& .categories': {
      display: 'flex',
      flexWrap: 'wrap',
      '& .category': {
        width: '50%',
        padding: 6,
        '& .img': {
          width: '80px',
          height: '80px',
          marginBottom: 4,
          borderRadius: 8,
          border: '1px solid #ededed',
          overflow: 'hidden',
        },
        '& img': {
          width: '100%',
          height: '100%',
          objectFit: 'cover',
        },
        '& p': {
          textAlign: 'center',
          fontFamily: 'Roboto',
          fontStyle: 'normal',
          fontWeight: 500,
          fontSize: 10,
          lineHeight: 1,
          color: '#1d1d1d',
          margin: 0,
        },
      },
    },
  },
}));

export default useStyles;