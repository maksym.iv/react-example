import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  autocomplete: {
    margin: '15px 0',
  },
  saveBtn: {
    margin: '15px 0',
  },
  clientPreviewArea: {
    height: '100%',
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#ededed',
  },
}));

export default useStyles;