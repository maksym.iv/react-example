import React, {
  useEffect,
  useReducer,
  useState,
  useCallback,
  useMemo,
} from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import _find from 'lodash/find';
import _isEqual from 'lodash/isEqual';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Paper,
  Grid,
  Switch,
  Button,
  TextField,
  FormControlLabel,
  FormControl,
  InputAdornment,
  IconButton,
  FormHelperText
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import {
  Close as CloseIcon,
  Link as LinkIcon,
  SaveAlt as SaveIcon,
} from '@material-ui/icons';
import {
  ConfirmDialog,
  Typography
} from 'components';
import { getImgName, showImageUrl, firstEntry } from 'utils/files';
import { ClientPreview } from './components/ClientPreview';

import useStyles from './styles';

const initialFormState = {
  bannerImage: '',
  categoryId: '',
  customLink: ''
};

const formReducer = (state, { field, value, newState }) => {
  if (field) {
    return {
      ...state,
      [field]: value
    };
  }
  return {
    ...state,
    ...newState
  };
};

const initialAutocompleteValue = {
  _id: '',
  name: '',
  section: '',
};

const linkRegEx = /[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/i;
const checkEmail = (email) => linkRegEx.test(email);

const MenuBanner = ({
  langs,
  menuBanner,
  isBannerVisible,
  saveMenuBanner,
  menuCategories,
  menuItems,
  saveAccountInfo,
  history
}) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [state, dispatch] = useReducer(formReducer, initialFormState);
  const [isEmailError, setIsEmailError] = useState(false);
  const [alertOpen, setAlertOpen] = useState(false);
  const [isBannerActive, setIsBannerActive] = useState(false);
  const [autocompleteValue, setAutocompleteValue] = useState(initialAutocompleteValue);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [path, setPath] = useState('');

  const isEqualInfo = _isEqual(state, menuBanner);
  // show modal if forget save data before move to another page
  useEffect(() => {
    const unblock = history.block(({ pathname }) => {
      setPath(pathname);
      if (isEqualInfo) return true;

      setIsModalOpen(true);
      return false;
    });

    return () => unblock();
  });

  const defaultLang = useMemo(() => (
    _get(_find(langs, ['index', 0]), 'lang') || _get(langs, '[0].lang')
  ), [langs]);

  const options = useMemo(() => {
    const customLinkValue = { _id: '0', name: t('menu.banner.customLink'), section: 'custom' };
    const categories = menuCategories
      .filter(category => menuItems.find(item => item.categoryId === category._id))
      .map(({ _id, name }) => ({
        _id,
        name: _get(name, defaultLang) || _get(name, firstEntry(name), ''),
        section: 'category'
      }));

    return [
      ...categories,
      customLinkValue,
    ];
  }, [defaultLang, menuCategories, menuItems, t]);

  useEffect(() => {
    dispatch({
      newState: {
        ...menuBanner
      }
    });
  }, [menuBanner]);
  // update state with value from store 
  useEffect(() => {
    setIsBannerActive(isBannerVisible);
  }, [isBannerVisible]);

  useEffect(() => {
    const currentValue = options.find(v => v._id === menuBanner.categoryId) || initialAutocompleteValue;

    setAutocompleteValue(currentValue);
  }, [menuBanner, options]);

  const onChange = useCallback((e) => {
    const field = _get(e, 'target.name', '');
    const value = _get(e, 'target.value', '');

    dispatch({ field, value });
  }, [dispatch]);

  const switchHandler = (e) => {
    const checked = _get(e, 'target.checked', false);

    setIsBannerActive(checked);
    saveAccountInfo({ isBannerActive: checked });
  }

  const onChangeByField = (field, value) => dispatch({ field, value });

  const handleChangeBanner = (event) => onChangeByField('bannerImage', event.target.files[0]);

  const chooseBannerLink = (e, val) => {
    const id = _get(val, '_id', '');

    setAutocompleteValue(val);
    onChangeByField('categoryId', id);
  };

  const clearBannerImage = () => setAlertOpen(prev => !prev);

  const deleteBanner = () => {
    clearBannerImage();
    onChangeByField('bannerImage', '');
  }

  const onSave = useCallback(() => {
    const isValid = checkEmail(state.customLink);

    if (state.customLink === '' || isValid) {
      setIsEmailError(false);
      saveMenuBanner(state);
    } else {
      setIsEmailError(true);
    }
  }, [state, saveMenuBanner]);

  const imgName = getImgName(state.bannerImage);

  const confirmModalHandler = () => {
    onSave();
    setIsModalOpen(false);
  }

  const cancelModalHandler = async () => {
    await dispatch({ newState: menuBanner });
    setIsModalOpen(false);
    history.push(path);
  }

  return (
    <Paper>
      <Box p={3} pl={2.5}>
        <Grid container spacing={4}>
          <Grid item xs>
            <Typography variant='h5' weight='medium' noWrap>
              {t('menu.banner.title')}
            </Typography>
          </Grid>
          <Grid item>
            <FormControlLabel
              label={isBannerActive ? t('common.labels.on') : t('common.labels.off')}
              name='isBannerActive'
              checked={isBannerActive}
              onChange={switchHandler}
              control={
                <Switch
                  color='primary'
                />
              }
            />
          </Grid>
        </Grid>

        <Grid container spacing={4}>
          <Grid item xs lg={6}>
            <Box pr={{ lg: 8, xl: 15 }}>
              <FormControl fullWidth>
                <TextField
                  id='banner-image'
                  color='primary'
                  variant='outlined'
                  label={t('menu.banner.forMenu')}
                  value={imgName}
                  InputLabelProps={{ shrink: true }}
                  InputProps={{
                    endAdornment:
                      <>
                        {imgName !== '' &&
                          <InputAdornment position='end'>
                            <IconButton
                              aria-label='toggle password visibility'
                              onClick={clearBannerImage}
                            >
                              <CloseIcon />
                            </IconButton>
                          </InputAdornment>
                        }
                        <InputAdornment position='end'>
                          <label htmlFor='bannerImage'>
                            <Button
                              variant='contained'
                              component='span'
                              color='primary'
                              startIcon={<SaveIcon />}
                            >
                              {t('common.btn.upload')}
                            </Button>
                          </label>
                        </InputAdornment>
                      </>
                  }}
                />
                <input
                  accept='image/*'
                  style={{ display: 'none' }}
                  id='bannerImage'
                  name='bannerImage'
                  type='file'
                  onChange={handleChangeBanner}
                />
                <FormHelperText id='bannerImage'>{t('menu.banner.recommendations')}</FormHelperText>
              </FormControl>

              <FormControl fullWidth className={classes.autocomplete}>
                <Autocomplete
                  name='categoryId'
                  value={autocompleteValue}
                  groupBy={({ section }) => t(`menu.sectionTitle.${section}`)}
                  options={options}
                  getOptionLabel={opt => opt.name}
                  getOptionSelected={opt => opt.name}
                  renderInput={(params) =>
                    <TextField
                      // eslint-disable-next-line react/jsx-props-no-spreading
                      {...params}
                      variant='outlined'
                      label={t('menu.banner.link')}
                    />
                  }
                  onChange={chooseBannerLink}
                />
              </FormControl>

              {state.categoryId === '0' &&
                <TextField
                  error={isEmailError}
                  helperText={isEmailError && t('menu.banner.wrongLinkFormat')}
                  id='custom-link'
                  label={t('menu.banner.customLink')}
                  fullWidth
                  variant='outlined'
                  name='customLink'
                  InputProps={{
                    startAdornment: <LinkIcon />,
                  }}
                  value={state.customLink}
                  onChange={onChange}
                />
              }

              {!_isEqual(state, menuBanner) &&
                <Button
                  className={classes.saveBtn}
                  variant='contained'
                  onClick={onSave}
                  color='primary'
                >
                  {t('common.btn.save')}
                </Button>
              }
            </Box>
          </Grid>

          <Grid item xs lg={6}>
            <div className={classes.clientPreviewArea}>
              <Typography variant='h5' weight='medium' color='text'>
                {t('profile.previewTitle')}
              </Typography>
              <ClientPreview
                bannerImage={showImageUrl(state.bannerImage)}
                categories={menuCategories}
              />
            </div>
          </Grid>
        </Grid>
      </Box>
      {alertOpen && (
        <ConfirmDialog
          open={alertOpen}
          title={t('menu.banner.deleteRowQuestion')}
          confirmBtnText={t('common.btn.delete')}
          cancelBtnText={t('common.btn.cancel')}
          onCancel={clearBannerImage}
          onConfirm={deleteBanner}
        />
      )}

      <ConfirmDialog
        open={isModalOpen}
        title={t('common.confirm.confirmChanges')}
        cancelBtnText={t('common.btn.confirm')}
        confirmBtnText={t('common.btn.cancel')}
        onConfirm={cancelModalHandler}
        onCancel={confirmModalHandler}
      />
    </Paper>
  );
};

MenuBanner.defaultProps = {
  langs: [],
  menuBanner: initialFormState,
  isBannerVisible: false,
  saveMenuBanner: () => { },
  menuCategories: [],
  menuItems: [],
  saveAccountInfo: () => { }
};

MenuBanner.propTypes = {
  langs: PropTypes.arrayOf(
    PropTypes.shape({
      lang: PropTypes.string,
      title: PropTypes.string
    })
  ),
  isBannerVisible: PropTypes.bool,
  saveMenuBanner: PropTypes.func,
  menuCategories: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      typeId: PropTypes.string,
      name: PropTypes.instanceOf(Object)
    })
  ),
  menuItems: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      categoryId: PropTypes.string,
      accountId: PropTypes.string,
      price: PropTypes.number,
      cookingTime: PropTypes.number,
      tags: PropTypes.arrayOf(PropTypes.string),
      images: PropTypes.arrayOf(PropTypes.string),
      content: PropTypes.instanceOf(Object)
    })
  ),
  menuBanner: PropTypes.instanceOf(Object),
  saveAccountInfo: PropTypes.func,
  history: PropTypes.shape({
    block: PropTypes.func,
    push: PropTypes.func
  }).isRequired
};

export default React.memo(MenuBanner);