import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { saveMenuBanner } from 'actions/menu';
import { saveAccountInfo } from 'actions/account';
import MenuBanner from './MenuBanner';

const mapStateToProps = ({
  menu: {
    menuCategories,
    menuBanner,
    menuItems,
  },
  account: {
    accountInfo: { isBannerActive: isBannerVisible },
  },
}) => ({
  menuBanner,
  menuCategories,
  menuItems,
  isBannerVisible,
});

const mapDispatchToProps = {
  saveMenuBanner,
  saveAccountInfo,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(withRouter(MenuBanner));
