import {
  MENU__SET_LOADING,
  MENU__ADD_BANNER
} from 'actionTypes/menu';
import _get from 'lodash/get';
import _omit from 'lodash/omit';
import _find from 'lodash/find';
import _isObject from 'lodash/isObject';

import { post, put } from 'utils/api';


import {
  SUCCESS_MESSAGE,
  BANNER_UPLOADED_MESSAGE
} from 'actionTypes/alerts';

import {
  setSuccessAlert,
  setErrorAlert
} from 'actions/alerts'

export const setLoading = (status) => (dispatch) => {
  dispatch({
    type: MENU__SET_LOADING,
    payload: status
  });
};

const addMenuBanner = (banner) => (dispatch) => {
  dispatch({
    type: MENU__ADD_BANNER,
    payload: banner
  });
};


export const saveMenuBanner = (data) => async (dispatch, getState) => {
  try {
    const {
      menu: {
        menuBanner
      }
    } = getState();
    const newData = {
      ...data
    };
    dispatch(setLoading(true));
    if (data.bannerImage && data.bannerImage.type) {
      const formData = new FormData();
      formData.append('file', data.bannerImage);
      const response = await post(
        'menu/uploadBannerImage',
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }
      );
      dispatch(setSuccessAlert(BANNER_UPLOADED_MESSAGE));
      newData.bannerImage = _get(response, 'bannerImage', '')
    }
    const savedAccount = await put(
      'menu/banner',
      {
        ...menuBanner,
        ...newData
      },
    );
    if (savedAccount) {
      dispatch(addMenuBanner(savedAccount));
      dispatch(setSuccessAlert(SUCCESS_MESSAGE));
    }
    dispatch(setLoading(false));
  } catch (e) {
    dispatch(setErrorAlert(e.message));
    dispatch(setLoading(false));
    console.error(e);
    throw e;
  }
};
